import { WebResysclagemPage } from './app.po';

describe('web-resysclagem App', function() {
  let page: WebResysclagemPage;

  beforeEach(() => {
    page = new WebResysclagemPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
