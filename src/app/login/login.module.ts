import { LoginService } from './login.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { FormBuilder, FormGroup, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { LoginComponent } from './login.component';
import { UsuarioService } from './../usuario/usuario.service';

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    ReactiveFormsModule
  ],
  declarations: [LoginComponent],
  providers: [UsuarioService, LoginService],
  exports: [
    LoginComponent
  ]
})
export class LoginModule { }
