import { DiaSemana } from './dia-semana';

export class HoraAtendimento {
    constructor(
        public diasSemana: DiaSemana[]
    ){}
}