export class MsgResponse{
    constructor(
        public action: string, 
        public status: boolean, 
        public message: string,
        public extra: any
    ){}
}