import { Categoria } from './categoria';

export class Residuo {

  constructor (
    public codR: number,
    public nome: string,
    public descricao: string,
    public imagemResiduo: string,
    public categoria: Categoria,
  ) {}

}