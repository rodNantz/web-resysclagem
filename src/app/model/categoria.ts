import { Residuo } from './residuo';
export class Categoria{
    constructor(
        public codC: number, 
        public nome: string, 
        public cor: string
    ){}
}