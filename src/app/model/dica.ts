import { Residuo } from './residuo';

export class Dica{
    constructor(
        public codD: number, 
        public nome: string, 
        public descricaoPath: string, 
        public isReutilizavel: Boolean, 
        public residuo: Residuo, 
        public anexos: Array<string>
    ){}
}