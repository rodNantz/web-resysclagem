import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Abas } from './abas.component';


@Component({
  selector: 'aba',
  templateUrl: './aba.component.html',
})
export class Aba {

  active = false;

  @Input() abaTitle: string;
  @Input() abaNome: string; //nome mais curto, pra usar no back-end
  @Output() notify: EventEmitter<string> = new EventEmitter<string>();

  onclick() {
    this.notify.emit('Click from nested componente');
  }

  constructor(abas:Abas) {
    abas.addAba(this);
   }
}
