import { NotificationsService } from 'angular2-notifications';
import { MsgResponse } from './../../model/msg-response';
import { Request } from './../../model/request';
import { Residuo } from './../../model/residuo';
import { PontoColeta } from './../../model/ponto-coleta';
import { HoraAtendimento } from './../../model/subclasses/hora-atendimento';
import { DiaSemana } from './../../model/subclasses/dia-semana';
import { PontodecoletaService } from './../pontodecoleta.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-listarponto',
  templateUrl: './listarponto.component.html',
  styleUrls: ['./listarponto.component.css']
})
export class ListarpontoComponent implements OnInit {

  constructor(private pcoService:PontodecoletaService,
              private nService:NotificationsService) { }

  //private nomesSemana = new Array;
  //private pontoHorario = new Array;

  //var publica para o clique
  public pontoAtual;
  public determinaPonto = (ponto) => {
    if (this.pontoAtual === ponto) return;
    this.pontoAtual = ponto;
  }

  ngOnInit() {
    this.pcoService.getPontodeColeta().subscribe(
      data => {
        this.pcoService.pontosDeColeta = JSON.parse(data.text());
        this.pcoService.atualizarPCOHorarios();
      });

  }

  trackByFn(index, ponto) {
    return ponto.codPC;
  }

  chamarEditar(pco:PontoColeta, i:number){
    this.pcoService.passarDadosEditar(pco, i);
  }

  deletaPonto(pco:PontoColeta, i){
    if(confirm("Deseja deletar o ponto "+ pco.codPC +"?")) {
      let request = new Request(JSON.stringify(pco));
      this.pcoService.deletaPontoDeColeta(request).subscribe(
        data => {
          let msg:MsgResponse = JSON.parse(data.text());
          if(msg.status){
            this.pcoService.pontosDeColeta.splice(i, 1);
            this.nService.success("Dica Deletada!", msg.message);
          } else {
            this.nService.error("Erro!", msg.message);
          }
        },
        error =>{
          this.nService.error("Erro!", error);
        }
      );
    }
  }

}
