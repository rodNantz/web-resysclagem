import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListarpontoComponent } from './listarponto.component';

describe('ListarpontoComponent', () => {
  let component: ListarpontoComponent;
  let fixture: ComponentFixture<ListarpontoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListarpontoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListarpontoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
