/// <reference types="@types/googlemaps" />
import { CookieService } from 'angular2-cookie/core';
import { Request } from './../../model/request';
import { ResiduoService } from './../../residuo/residuo.service';
import { Categoria } from './../../model/categoria';
import { CategoriaService } from './../../categoria/categoria.service';
import { HoraAtendimento } from './../../model/subclasses/hora-atendimento';
import { DiaSemana } from './../../model/subclasses/dia-semana';
import { PontoColeta } from './../../model/ponto-coleta';
import { PontodecoletaService } from './../pontodecoleta.service';
import { Component, ElementRef, NgZone, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, FormsModule, ReactiveFormsModule, Validators }
from '@angular/forms';
import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from "@angular/http";
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import { MapsAPILoader } from '@agm/core';
import { PropertiesService } from "app/properties.service";
import { NotificationsService } from "angular2-notifications";
import { UploadService } from "app/upload/upload.service";
import { MsgResponse } from "app/model/msg-response";
import { Residuo } from "app/model/residuo";

@Component({
  selector: 'app-cadastrarponto',
  templateUrl: './cadastrarponto.component.html',
  styleUrls: ['./cadastrarponto.component.css']
})
export class CadastrarpontoComponent implements OnInit {
  cadastrarPontoForm: FormGroup;
  public nome;
  public file: File;
  public descricao;
  public telefone;
  public email;
  public diaCheck: boolean[] = [false, false, false, false, false, false, false];
  public horaAbre: string[] = ["","","","","","",""];
  public horaFecha: string[] = ["","","","","","",""];
  //public residuos: Residuo[];
  public checkResiduos = new Array<Boolean>();

  // mapa
  public endereco = "";
  public latitude;
  public longitude;
  public default_coords = [39.8282, -98.5795];

  public searchControl: FormControl;
  public zoom: number;
  //
  private nomesSemana = new Array;

  public inputOkColor = "#FFF";
  public inputErrColor = "#FAA";

  @ViewChild("search")
  public searchElementRef: ElementRef;

  constructor(
    public fb: FormBuilder,
    private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone,
    private pontoService: PontodecoletaService,
    private residuoService: ResiduoService,
    private uploadService: UploadService,
    private properties: PropertiesService,
    private cookieS: CookieService,
    private nService: NotificationsService
  ) { }

  ngOnInit() {
    if(this.residuoService.residuos == undefined){
      this.residuoService.getResiduo().subscribe(
        data => {
          this.residuoService.residuos = JSON.parse(data.text());
          for(let residuo of this.residuoService.residuos){
            this.checkResiduos.push(false);
          }
        },
        error => {
          console.log("Erro ao listar residuos: " + error );
        });
    } else {
      for(let residuo of this.residuoService.residuos){
            this.checkResiduos.push(false);
      }
    }

    this.nomesSemana = ["Domingo","Segunda","Terça","Quarta","Quinta","Sexta","Sábado"];
    this.cadastrarPontoForm = this.fb.group({
      nome: ["", [Validators.required, Validators.maxLength(60)] ],
      complemento: ["", Validators.required],
      descricao: ["", [Validators.required, Validators.maxLength(300)] ],
      telefone: ["", [Validators.required, Validators.maxLength(20)] ],
      email: ["", [Validators.required, Validators.maxLength(70)] ]
    });


    // mapa

    //set google maps defaults
    this.zoom = 4;
    this.latitude = this.default_coords[0];
    this.longitude = this.default_coords[1];

    //create search FormControl
    this.searchControl = new FormControl();

    //set current position
    this.setCurrentPosition();

    //load Places Autocomplete
    this.mapsAPILoader.load().then(() => {
      let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
        types: ["address"]
      });
      autocomplete.addListener("place_changed", () => {
        this.ngZone.run(() => {
          //get the place result
          let place: google.maps.places.PlaceResult = autocomplete.getPlace();

          //verify result
          if (place.geometry === undefined || place.geometry === null) {
            this.endereco = "";
            return;
          }

          //set latitude, longitude and zoom
          this.latitude = place.geometry.location.lat();
          this.longitude = place.geometry.location.lng();
          //end completo
          this.endereco = place.formatted_address;
          this.searchControl.setValue(this.endereco);

          this.zoom = 12;
        });
      });
    });
  }

  private setCurrentPosition() {
    if ("geolocation" in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.latitude = position.coords.latitude;
        this.longitude = position.coords.longitude;
        this.zoom = 12;
      });
    }
  }

  private isLocationSelected(){
    if(this.endereco === "" || this.searchControl.value != this.endereco)
      return false;
    return true;
  }

  onChangeImage(event: EventTarget) {
      let eventObj: MSInputMethodContext = <MSInputMethodContext> event;
      let target: HTMLInputElement = <HTMLInputElement> eventObj.target;
      let files: FileList = target.files;
      this.file = files[0];
  }

  trackByFn(index, ponto) {
    return ponto.codPC;
  }

  private validatePonto(ponto: PontoColeta){
    /*
      Atributos obrigatórios: nome, endereco, telefone, resíduos, latitude e longitude. (@giu: revisar se é isso msm)
      Adaptar banco e WS pra que respeitem isso
    */
    //nome
    if(ponto.nome === ""){
      this.nService.alert("Atenção","'Nome' vazio!");
      return false;
    }
    //endereço (e coords)
    if(this.isLocationSelected()) {
      this.nService.alert("Atenção","Endereço vazio ou inválido!");
      //console.log("End: " + this.endereco + " | " + this.searchControl.value);
      return false;
    }
    //resíduos
    if(ponto.residuos.length === 0){
      this.nService.warn("Atenção","Nenhum resíduo selecionado!");
      return false;
    }
    for(let r of ponto.residuos){
      if(r == undefined){
        this.nService.warn("Atenção","Resíduo não selecionado!");
        return false;
      }
    }
    //horas - se checado, devem estar preenchidas
    for(let i of [0,1,2,3,4,5,6]) {
      if(this.diaCheck[i]) {
        if ( this.horaAbre[i] == undefined || this.horaAbre[i] == ""
          || this.horaFecha[i] == undefined || this.horaFecha[i] == "" ){
            this.nService.alert("Atenção","Corrija os horários de atendimento!");
            return false;
        }
      }
    }
    
    if (!this.cadastrarPontoForm.valid) {
      this.nService.alert("Atenção","Corrija os pontos em destaque!");
      return false;
    }

    // Angular native validations

    //outras vals
    return true;
  }

  cadastroPonto(pontoForm){
    //console.log("pontoForm: " + JSON.stringify(pontoForm));
    let ponto = pontoForm as PontoColeta;
    if(this.file != undefined){
      ponto.pathImagem = this.file.name;
    }
    ponto.latitude = this.latitude;
    ponto.longitude = this.longitude;
    ponto.endereco = this.endereco;
    ponto.horaAtendimento = new HoraAtendimento(new Array<DiaSemana>());
    for(let i of [0,1,2,3,4,5,6]) {
      if(this.diaCheck[i]) {
        ponto.horaAtendimento.diasSemana.push( new DiaSemana(i+1, this.horaAbre[i], this.horaFecha[i]) );
      }
    }
    ponto.residuos = new Array<Residuo>();
    let i = 0;
    for(let residuo of this.residuoService.residuos){
      if (this.checkResiduos[i]){
        ponto.residuos.push(residuo);
      }
      i++;
    }
    //ponto.residuos = this.residuos;
    //TODO: pegar residuos

    if(this.validatePonto(ponto)){
      //inserção no bd
      let request = new Request(JSON.stringify(ponto));
      let pco: PontoColeta = null;
      this.pontoService.novoPontodeColeta(request).subscribe(
        data => {
          let msg:MsgResponse = JSON.parse(data.text());
          if(msg.status){
            this.nService.success("Ponto de Coleta", msg.message);
            pco = msg.extra;
            this.pontoService.pontosDeColeta.push(pco);
          } else {
            this.nService.error("Ponto de Coleta", msg.message);
          }
        },
        error => {
          // 
          this.nService.error("PCO - erro!", error);
        }
      )

      if(pco != null){
        //depois, up imagem
        let entered = false;
        let msg:MsgResponse = new MsgResponse("upload", false, "Erro interno", null);
        let postData = {cod:pco.codPC, tipo:"PCO", id:this.cookieS.get("id"), t:this.cookieS.get("token")};
        this.uploadService.postWithFile(null, postData, this.file)
          .then((response) => {
            msg = JSON.parse(JSON.stringify(response));
            if(msg.status){
              //console.log("Fileposting success: " + JSON.stringify(msg.message));
              this.nService.success("Upload", msg.message);
              this.cadastrarPontoForm.reset();
            } else {
              this.nService.error("Upload", msg.message);
            }
          })
          .catch((error) => {
            this.nService.error("Upload", msg.message);
          });


      } else {
        this.cadastrarPontoForm.reset();
      }

    }

  }

}

