import { Residuo } from 'app/model/residuo';
import { DiaSemana } from './../model/subclasses/dia-semana';
import { HoraAtendimento } from './../model/subclasses/hora-atendimento';
import { Request } from './../model/request';
import { PropertiesService } from 'app/properties.service';
import { PontoColeta } from './../model/ponto-coleta';
import { Injectable, EventEmitter } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';

@Injectable()
export class PontodecoletaService {
  public pontosDeColeta = new Array<PontoColeta>();
  public pontoHorario = new Array;
  public nomesSemana = ["Domingo","Segunda","Terça","Quarta","Quinta","Sexta","Sábado"];

  public pontosDeColetaPendentes = new Array<PontoColeta>();
  public pontoHorarioPend = new Array;

  public pcoEditEmitter = new EventEmitter();
  public pcoEmEditar:PontoColeta = new PontoColeta(0, "", "", "", "", "", "", 
                                                   new HoraAtendimento(new Array<DiaSemana>()),
                                                   new Array<Residuo>(),
                                                   "", "", "", false);

  public pcoEmEditarIdx: number;

  private url:string;

  constructor(private http:Http,
              private properties:PropertiesService) { 
    //this.url = properties.getProp("ws.url") + "/server/pontocoleta";
    let urlKey = "ws.url";
    if(properties.getProp("prod") as Boolean){
      urlKey += ".prod";
    }
    this.url = properties.getProp(urlKey) + "/server/pontocoleta";
  }

  getPontodeColeta(){
    return this.http.get(this.url + "/lista").map((res: Response) => res);
  }

  getPontosdeColetaPendentes(){
    return this.http.get(this.url + "/pendente").map((res: Response) => res);
  }

  novoPontodeColeta(request:Request){
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    let body = JSON.stringify(request);
    return this.http.post(this.url, body, options).map((res:Response) => res);
  }

  passarDadosEditar(pco:PontoColeta, i:number){
    //console.log("editar pco: " + JSON.stringify(pco));
    this.pcoEmEditar = pco;
    this.pcoEmEditarIdx = i;
    this.pcoEditEmitter.emit("edit-pco");  //listener pra mudanças - passando o nome da aba a ser chamada
  }

  updatePontodeColeta(request:Request){
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    let body = JSON.stringify(request);
    return this.http.put(this.url, body, options).map((res:Response) => res);
  }

  deletaPontoDeColeta(request:Request){
    let pco:PontoColeta = JSON.parse(request.objeto);
    let urlParams = "?cod=" + pco.codPC
                  + "&id=" + request.id 
                  + "&t=" + request.token;
    return this.http.delete(this.url + urlParams).map((res:Response) => res);
  }

  atualizarPCOHorarios(){
    for(let p of this.pontosDeColeta) {
          let i = p.codPC;
          if(p.horaAtendimento != undefined) {
            for(let diaSemana of p.horaAtendimento.diasSemana) {
              if(this.pontoHorario[i] == null){
                this.pontoHorario[i] = "";
              }
              let concat = this.pontoHorario[i];
              this.pontoHorario[i] = concat 
                                      + this.nomesSemana[diaSemana.dia - 1] + ", "
                                      + diaSemana.abertura + " às "
                                      + diaSemana.encerramento + ". ";
            }
            //console.log("pontoHorario: ");
            //console.log(JSON.stringify(this.pontoHorario[i]));
          }
    }
  }

  atualizarPCOHorariosPend(){
    for(let p of this.pontosDeColetaPendentes) {
          let i = p.codPC;
          if(p.horaAtendimento != undefined) {
            for(let diaSemana of p.horaAtendimento.diasSemana) {
              if(this.pontoHorarioPend[i] == null){
                this.pontoHorarioPend[i] = "";
              }
              let concat = this.pontoHorarioPend[i];
              this.pontoHorarioPend[i] = concat 
                                      + this.nomesSemana[diaSemana.dia - 1] + ", "
                                      + diaSemana.abertura + " às "
                                      + diaSemana.encerramento + ". ";
            }
            //console.log("pontoHorarioPend: ");
            //console.log(JSON.stringify(this.pontoHorarioPend[i]));
          }
    }
  }

}
