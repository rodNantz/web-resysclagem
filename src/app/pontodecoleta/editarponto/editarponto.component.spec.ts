import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditarpontoComponent } from './editarponto.component';

describe('EditarpontoComponent', () => {
  let component: EditarpontoComponent;
  let fixture: ComponentFixture<EditarpontoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditarpontoComponent ]
    })
    .compileComponents();
  }));
 
  beforeEach(() => {
    fixture = TestBed.createComponent(EditarpontoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
