import { TestBed, inject } from '@angular/core/testing';

import { PontodecoletaService } from './pontodecoleta.service';

describe('PontodecoletaService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PontodecoletaService]
    });
  });

  it('should ...', inject([PontodecoletaService], (service: PontodecoletaService) => {
    expect(service).toBeTruthy();
  }));
});
