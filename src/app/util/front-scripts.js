Zepto(function(){

	$(document).on('load', 'tabs nav li', function(){
		TrocaAbaAtiva();
	})

	$(document).on('click', 'tabs nav li' , function(){
		TrocaAbaAtiva();
	});

	//troca aba ativa
	function TrocaAbaAtiva() {
		$('nav ul li.ativo').removeClass('ativo');

		//Pega a div ativa
		var divAtiva = $('.tabContentWrapper tab[tabtitle] > div').not('[hidden]').parent().attr('tabtitle');

		//Verifica qual aba está visivel
		$('nav ul li').each(function(){
			if ( $(this).text().trim() == divAtiva ){
				$(this).addClass('ativo');
		    }
		});
	}

	setInterval(function(){
		vScroll = $(window).scrollTop();
		if(vScroll > 75){
			$('nav').addClass('fixed');
		} else {
			$('nav').removeClass('fixed');
		}
	},10)
});