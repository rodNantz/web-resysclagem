import { PropertiesService } from './properties.service';
import { LoginService } from './login/login.service';
import {CookieService} from 'angular2-cookie/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, Input, AfterViewInit, OnInit } from '@angular/core';
//import { ToastsManager } from 'ng2-toastr/ng2-toastr';

//import { UsuariocComponent } from "./usuario/usuarioc.component";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'R e s y s c l a p p';

  public toastOptions = {
    position: ["bottom", "right"],
    timeOut: 5000,
    lastOnBottom: true
  }

  constructor(private router: Router,
              public route: ActivatedRoute,
              public loginS: LoginService,
              public pService: PropertiesService) {
    if(!pService.getProp("prod")){
      this.toastOptions.timeOut = 0;
    }
  }

  ngOnInit() {
    if (this.loginS.isLogged()) {
      if (this.router.url != "/logged") this.router.navigate(['./logged']);
    } else {
      if (this.router.url != "/login") this.router.navigate(['./login']);
    }

  }

}
