import { Request } from './../../model/request';
import { NotificationsService } from 'angular2-notifications';
import { MsgResponse } from './../../model/msg-response';
import { Categoria } from './../../model/categoria';
import { CategoriaService } from './../categoria.service';
import { Component, OnInit } from '@angular/core';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { NgModule } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { Http, Response, Headers, RequestOptions } from "@angular/http";
import 'rxjs/add/operator/map';


@Component({
  selector: 'app-listarcategoria',
  templateUrl: './listarcategoria.component.html',
  styleUrls: ['./listarcategoria.component.css']
})
export class ListarcategoriaComponent implements OnInit {  
  private postResponse: Response;
  private termo;
  private tinycolor;

  constructor(private service: CategoriaService,
              private nService: NotificationsService) {
    this.service = service;
    this.tinycolor = require("tinycolor2");
  }

  ngOnInit() {
    
    //this.residuos = this.service.getResiduo();
    this.service.getCategorias().subscribe(
      data => {
        this.service.categorias = data;
      });
  }

  isColorLight(color): boolean { 
    if (this.tinycolor(color).isLight()) {
      return true;
    }
    return false;
  }

  trackByFn(index, categoria) {
    return categoria.codC;
  }

  chamarEditar(cat:Categoria, i:number){
    this.service.passarDadosEditar(cat, i);
  }

  removerCategoria(categoria, i){
    if(confirm("Você tem certeza que deseja deletar esta categoria: " + categoria.nome + "?")){
      let request:Request = new Request(JSON.stringify(categoria));
      this.service.deletaCategoria(request).subscribe( // antes tava só residuo no parametro
        data =>{
          let msg:MsgResponse = data as MsgResponse;
          if(msg.status){
            this.nService.success("Categoria", msg.message);
            this.service.categorias.splice(i, 1);
          } else {
            this.nService.error("Categoria", msg.message);
          }
        },
        error => {
          this.nService.error("Categoria", error);
        }
      );
    }
  }
}
