import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Component, Input } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

export class Prop {
  constructor(
    public key, 
    public value
  ) {}
  
}

@Injectable()
export class PropertiesService {
  private props = new Array<Prop>();
  
  constructor(private http:Http) {
  }
    
  public load() {
        return new Promise((resolve, reject) => {
            this.http.get('./assets/properties.json').map( res => res.json() ).catch((error: any):any => {
                console.log('Configuration file could not be read');
                resolve(true);
                return Observable.throw(error.json().error || 'Server error');
            }).subscribe( (jsonResponse) => {
                //this.props = jsonResponse;
                let request:any = null;

                request = this.http.get('./assets/properties.json');
                
                if (request) {
                    request
                        .map( res => res.json() )
                        .catch((error: any) => {
                            console.error('Error reading configuration file');
                            resolve(error);
                            return Observable.throw(error.json().error || 'Server error');
                        })
                        .subscribe((responseData) => {
                            this.props = responseData;
                            resolve(true);
                        });
                } else {
                    console.error('Env config file "env.json" is not valid');
                    resolve(true);
                }
            });

        });
    }

  init(){
    this.get().subscribe(
      data => {
        this.props = data as Array<Prop>;
      }, 
      error => {
        console.log("Props erro: " + error);
    });
  }

  private get() : Observable<any> {
      return this.http.get('./assets/properties.json')
      .map((res:any) => res.json());
  }

  public getProp(myKey: string) {
    for(let prop of this.props){
      if(prop.key as string == myKey){
        return prop.value;
      }
    }
    return undefined;
  }
}


