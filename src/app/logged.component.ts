import { NotificationsService } from 'angular2-notifications';
import { LoginService } from './login/login.service';
import { CookieService } from 'angular2-cookie/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-logged',
  templateUrl: './logged.component.html',
  styleUrls: ['./logged.component.css']
})
export class LoggedComponent implements OnInit {

  constructor(private router:Router,
              private route:ActivatedRoute,
              private cService: CookieService,
              private lService: LoginService,
              private nService: NotificationsService) { }

  ngOnInit() {
    if(!this.lService.isLogged){
      this.router.navigate(['/login']);
    }
    /* OFF - para checar se WS ligado, ainda n funciona
    if(! this.lService.isWSLigado()) {
      this.nService.alert("WS Fora", "Não foi possível se conectar com o serviço!");
    }
    */
  }

}
