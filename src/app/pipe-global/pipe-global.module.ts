import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FiltroPipeC } from "app/pipe-global/filtro.pipe";
import { FiltroPipeD, FiltroPipeP, FiltroPipeR, FiltroPipeSubR } from "app/pipe-global/filtro.pipe";

@NgModule({
  
  declarations: [FiltroPipeC, FiltroPipeD, FiltroPipeP, FiltroPipeR, FiltroPipeSubR ],
  imports : [CommonModule],
  exports: [FiltroPipeC, FiltroPipeD, FiltroPipeP, FiltroPipeR, FiltroPipeSubR ],
  providers: []

})
export class PipeGlobalModule { }
