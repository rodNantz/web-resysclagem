import { DataLabel } from './model/data-label';
import { NotificationsService } from 'angular2-notifications';
import { DatePipe } from '@angular/common';
import { Request } from './../model/request';
import { RelatorioService } from './relatorio.service';
import { RelatorioModel } from './model/relatorio-model';
import { Relatorio } from './model/relatorio';
import { Component, OnInit, NgZone } from '@angular/core';
import { ChangeDetectorRef, SimpleChanges } from '@angular/core';        

@Component({
  selector: 'app-relatorio',
  templateUrl: './relatorio.component.html',
  styleUrls: ['./relatorio.component.css']
})
export class RelatorioComponent implements OnInit {
  public ordemMaiores;
  public ordemLabel;
  public dataInicial;
  public dataFinal;

  constructor(private ref: ChangeDetectorRef,
            private service: RelatorioService,
            private nService: NotificationsService,
            private datePipe: DatePipe){
    this.ordemMaiores = true;
    this.ordemLabel = ["Maiores","DESC"];
  }

  ngOnInit(){
    this.service.tiposRelatorio().subscribe(
      res => {
        this.service.optSolicitar = JSON.parse(res.text());
        this.service.selSolicitada = this.service.optSolicitar[0];
      }, error => {
        console.log(error);
      }
    )

 }

 public onChangeTipo(opt) {
    this.service.selSolicitada = opt;
    //console.log("mudou: " + JSON.stringify(this.service.selSolicitada));
    //this.relatorio.update(rel.barChartLabels, rel.barChartData);
  }

  public onChangeDate(dateI, dateF) {
    if(dateI != 0){
      this.dataInicial = dateI;
    }
    if(dateF != 0){
      this.dataFinal = dateF;
    }
  }

  public onChangeOrdem(opt) {
    if(this.ordemMaiores == true){
      this.ordemLabel = ['Maiores', 'DESC'];
    } else {
      this.ordemLabel = ['Menores', 'ASC'];
    }
  }

  atualizar(){
    if(this.dataInicial == undefined){
      this.dataInicial = '1900-01-01';
    }
    if(this.dataFinal == undefined){
      this.dataFinal = "2999-12-31";
    }
    let relatorio = new Relatorio(this.service.selSolicitada.key, "", this.service.selSolicitada.extra, 
                                  this.ordemLabel[1], this.dataInicial, this.dataFinal, 
                                  this.service.selSolicitada.extra2,
                                  new Array<any>(), new Array<DataLabel>());
    let request:Request = new Request(JSON.stringify(relatorio));
    this.service.mostrarGrafico = false;

    this.service.gerarRelatorio(request).subscribe(
      res => {
        let rel:Relatorio = JSON.parse(res.text());
        //console.log(JSON.stringify(rel));
        if(rel.dadoVertical.length == 0){
          this.nService.alert("Relatório", "Nenhum registro encontrado!");
        } else {
          this.service.rData.dadoVertical = rel.dadoVertical;      
          this.service.rData.dadoHorizontal = rel.dadoHorizontal;
          
          this.service.rData.ignoraDatas = rel.ignoraDatas;
          this.service.rData.nome = rel.nome;
          this.service.relatorioAtual.setData(this.service.rData);
          this.service.mostrarGrafico = true;
        }
      }, error => {
        this.nService.error("Relatório", error);
        console.log(error);
      }
    );
  
  }

}
