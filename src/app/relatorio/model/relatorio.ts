import { DataLabel } from './data-label';
export class Relatorio{
    constructor(
        public codRel: number, 
        public nome: string,
        public query: string,
        public ordem: string,
        public dataInicial: string,
        public dataFinal: string,
        public ignoraDatas: boolean,
        public dadoVertical: Array<any>,
        public dadoHorizontal: DataLabel[]
    ){}
}