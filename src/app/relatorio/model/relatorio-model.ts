import { OnInit } from '@angular/core';
import { Relatorio } from 'app/relatorio/model/relatorio';

export class RelatorioModel {

    public codRel: number
    public nome: string;
    public omitirDatas: boolean = false;
    //public dataEmissao: string;
    public barChartOptions:any;
    public barChartLabels;
    public barChartType: string = 'horizontalBar';
    public barChartLegend:boolean = true;
    // horizontal
    public barChartData;

    constructor(rel:Relatorio) {
        this.setData(rel);
    }

    setData(rel:Relatorio){
        this.barChartOptions = {
            scaleShowVerticalLines: false,
            responsive: true
        };
        this.codRel = rel.codRel;
        this.barChartType = 'horizontalBar';

        this.barChartLabels = rel.dadoVertical;
        this.barChartData = rel.dadoHorizontal;
        this.nome = rel.nome;
        this.omitirDatas = rel.ignoraDatas;
    }

    // events
    public chartClicked(e:any):void {
        console.log(e);
    }

    public chartHovered(e:any):void {
        console.log(e);
    }

    public update(labels, data):void {
        
        this.barChartLabels = labels;
        this.barChartData = data;

        /**
         * (My guess), for Angular to recognize the change in the dataset
         * it has to change the dataset variable directly,
         * so one way around it, is to clone the data, change it and then
         * assign it;
         */
    }

}