import { RelatorioService } from './relatorio.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { FormBuilder, FormGroup, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { RelatorioComponent } from './relatorio.component';
import { PipeGlobalModule } from "app/pipe-global/pipe-global.module";
import { MenuModule } from "app/menu/menu.module";
//Gráficos
import { ChartsModule } from 'ng2-charts';

@NgModule({
  imports: [
    CommonModule,
    PipeGlobalModule,
    MenuModule,
    FormsModule,
    ChartsModule
  ],
  declarations: [RelatorioComponent],
  exports: [RelatorioComponent],
  providers: [RelatorioService]
})
export class RelatorioModule { }
