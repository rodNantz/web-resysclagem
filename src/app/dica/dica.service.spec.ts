import { TestBed, inject } from '@angular/core/testing';

import { DicaService } from './dica.service';

describe('DicaService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DicaService]
    });
  });

  it('should ...', inject([DicaService], (service: DicaService) => {
    expect(service).toBeTruthy();
  }));
});
