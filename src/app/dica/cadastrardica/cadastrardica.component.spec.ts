import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CadastrardicaComponent } from './cadastrardica.component';

describe('CadastrardicaComponent', () => {
  let component: CadastrardicaComponent;
  let fixture: ComponentFixture<CadastrardicaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CadastrardicaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CadastrardicaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

