import { CookieService } from 'angular2-cookie/core';
import { Request } from './../../model/request';
import { ListardicaComponent } from './../listardica/listardica.component';
import { UploadService } from './../../upload/upload.service';
import { MsgResponse } from './../../model/msg-response';
import { NotificationsService } from 'angular2-notifications';
import { Dica } from './../../model/dica';
import { DicaService } from './../dica.service';
import { Residuo } from './../../model/residuo';
import { ResiduoService } from './../../residuo/residuo.service';
import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, FormsModule, ReactiveFormsModule, Validators, FormArray }
        from '@angular/forms';
import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from "@angular/http";
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';

@Component({
  selector: 'app-cadastrardica',
  templateUrl: './cadastrardica.component.html',
  styleUrls: ['./cadastrardica.component.css']
})
export class CadastrardicaComponent implements OnInit {
  @Input()
  listaDica:ListardicaComponent;

  //private residuos;
  dicaForm: FormGroup;
  private codD;
  private isReutilizavel;
  private residuo: Residuo;
  private titulodica;

  //inputs html dica
  intCount = 0;
  qtdInput = [];
  private files: File[] = [];
  public anexos: string[] = [];

  public myForm: FormGroup;

  constructor(private _fb: FormBuilder,
              private residuoService: ResiduoService,
              private dicaService: DicaService,
              private upService: UploadService,
              private cookieS: CookieService,
              private nService: NotificationsService) { }

  ngOnInit() {
      if(this.residuoService.residuos == undefined){
        this.residuoService.getResiduo().subscribe(
        data => {
          this.residuoService.residuos = JSON.parse(data.text());
        });
      }


      this.myForm = this._fb.group({
            codD: [0, Validators],
            nome: ["", Validators],
            residuo: [null, Validators],
            isReutilizavel: [false, Validators],
            anexos0: ["", Validators],
            anexos1: ["", Validators],
            anexos2: ["", Validators],
            anexos3: ["", Validators],
            anexos4: ["", Validators],
            anexos5: ["", Validators],
            anexos6: ["", Validators],
            anexos7: ["", Validators],
            anexos8: ["", Validators],
            anexos9: ["", Validators],
            anexos10: ["", Validators],
            anexos11: ["", Validators],
            anexos12: ["", Validators],
            anexos13: ["", Validators],
            anexos14: ["", Validators]
      });
        this.qtdInput[this.intCount] = this.intCount;
        this.intCount++;
  }

  onChange(event: EventTarget, index: number) {
        let eventObj: MSInputMethodContext = <MSInputMethodContext> event;
        let target: HTMLInputElement = <HTMLInputElement> eventObj.target;
        let files: FileList = target.files;
        this.files[index] = files[0];
    }

  add(){
    if(this.intCount < 15) {
      this.qtdInput[this.intCount] = this.intCount;
      this.intCount++;
    }
  }

  del(){
    if(this.intCount > 0 ){
      this.qtdInput.splice(this.intCount, 1);
      this.intCount--;
    }
  }

  trackByFn(index, item) {
    return index;
  }

  cadastraDica(dicaForm){
    let dica:Dica = new Dica(0,"","",false,null,[]);
    dica.nome = this.myForm.controls["nome"].value;
    dica.residuo = this.myForm.controls["residuo"].value as Residuo;
    dica.isReutilizavel = this.myForm.controls["isReutilizavel"].value as Boolean;
    let pairCount = 0;
    for(let i of this.qtdInput){
      dica.anexos[pairCount] = this.files[i].name;
      dica.anexos[pairCount+1] = this.myForm.controls["anexos".concat(i)].value;
      pairCount = pairCount + 2;
    }
    //WS: dica

    let upload = true;
    let dicaSeInserida: Dica;
    let request = new Request(JSON.stringify(dica));
    this.dicaService.novaDica(request).subscribe(
      data =>{
        let msg:MsgResponse = JSON.parse(data.text());
        if(msg.status){
          this.nService.success("Dica", msg.message);
          dicaSeInserida = JSON.parse(msg.extra);
          this.dicaService.dicas.push(dicaSeInserida);
          //WS: upload IMG
          let entered = false;
          let postData = {cod:dicaSeInserida.codD, tipo:"DIC", id:this.cookieS.get("id"), t:this.cookieS.get("token")};
          for(let file of this.files) {
            this.upService.postWithFile(null, postData, file)
              .then((response) => {
                let msg2:MsgResponse = JSON.parse(JSON.stringify(response));
                entered = true;
                if(msg2.status){
                  console.log("Fileposting success: " + msg2.message);
                } else {
                  console.log("Fileposting error: " + msg2.message);
                }
              })
              .catch((error) => {
                console.log("Fileposting error: " + error);
              });
          }
          // fim upload imgs dica
        } else {
          this.nService.error("Dica", msg.message);
        }
      },
      error => {
        this.nService.error("Dica - erro!", error);
        console.log("Erro no cadastro da dica: " + JSON.stringify(dica));
      }
    )

  }


}
