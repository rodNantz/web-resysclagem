import { Component, OnInit } from '@angular/core';
import { ListardicaComponent } from './listardica/listardica.component';
import { CadastrardicaComponent } from './cadastrardica/cadastrardica.component';
import { EditardicaComponent } from './editardica/editardica.component';

@Component({
  selector: 'app-dica',
  templateUrl: './dica.component.html',
  styleUrls: ['./dica.component.css']
})
export class DicaComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
