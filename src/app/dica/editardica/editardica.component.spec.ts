import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditardicaComponent } from './editardica.component';

describe('EditardicaComponent', () => {
  let component: EditardicaComponent;
  let fixture: ComponentFixture<EditardicaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditardicaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditardicaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
