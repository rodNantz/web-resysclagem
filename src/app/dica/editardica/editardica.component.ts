import { CookieService } from 'angular2-cookie/core';
import { UploadService } from './../../upload/upload.service';
import { MsgResponse } from './../../model/msg-response';
import { Request } from './../../model/request';
import { Categoria } from './../../model/categoria';
import { Dica } from './../../model/dica';
import { ResiduoService } from './../../residuo/residuo.service';
import { NotificationsService } from 'angular2-notifications';
import { Residuo } from 'app/model/residuo';
import { DicaService } from './../dica.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, FormsModule, ReactiveFormsModule, Validators, FormArray }
        from '@angular/forms';

@Component({
  selector: 'app-editardica',
  templateUrl: './editardica.component.html',
  styleUrls: ['./editardica.component.css']
})
export class EditardicaComponent implements OnInit {
  private residuos;
  dicaForm: FormGroup;
  private codD;
  private isReutilizavel;
  private residuo: Residuo;
  private titulodica;

  //inputs html dica
  intCount = 0;
  qtdInput = [];
  private fileNomesAntes: string[] = [];
  private files: File[] = [];
  public anexos: string[] = [];

  public myForm: FormGroup;

  constructor(private fb: FormBuilder,
              private service: DicaService,
              private residuoService: ResiduoService,
              private upService: UploadService,
              private cookieS: CookieService,
              private nService: NotificationsService) { }

  ngOnInit() {
    if(this.residuoService.residuos == undefined){
      this.residuoService.getResiduo().subscribe(
      res => {
        this.residuoService.residuos = res.json();
        //console.log(res.text());
      }, error => {
        console.log(error);
      });
    }

    if(this.service.dicaEmEditar.residuo == undefined){
      this.service.dicaEmEditar.residuo = new Residuo(0, '', '', '', new Categoria(0, '', ''));
    }

    this.myForm = this.fb.group({
      codD: [0, Validators],
      nome: ["", Validators],
      residuo: ["", Validators],
      isReutilizavel: [false, Validators],
      anexos0: ["", Validators],
      anexos1: ["", Validators],
      anexos2: ["", Validators],
      anexos3: ["", Validators],
      anexos4: ["", Validators],
      anexos5: ["", Validators],
      anexos6: ["", Validators],
      anexos7: ["", Validators],
      anexos8: ["", Validators],
      anexos9: ["", Validators],
      anexos10: ["", Validators],
      anexos11: ["", Validators],
      anexos12: ["", Validators],
      anexos13: ["", Validators],
      anexos14: ["", Validators]
    });

  }

  editarDicaSetup(event) {
    //inicializando vars
    this.intCount = 0;
    this.qtdInput = [];
    this.files = [];
    this.anexos = [];
    //preenchendo anexos da dica
    let dica:Dica = this.service.dicaEmEditar;
    if(dica.codD !== 0 && dica.anexos != null){
      let imgOuTxtCnt = 1;
      let imgCnt = 0;
      for(let anexo of dica.anexos) {
        //console.log(anexo);
        if(imgOuTxtCnt % 2 != 0) {    //ímpar
          // imagem
          this.fileNomesAntes[imgCnt] = anexo;
          imgCnt++;
        } else {
          // texto
          this.myForm.controls["anexos".concat(this.intCount.toString())].setValue(anexo);
          this.anexos[this.intCount] = anexo;
          //contador: um por par img/txt
          this.qtdInput[this.intCount] = this.intCount;
          this.intCount++;
        }
        imgOuTxtCnt++;
      }
    }
  }

  trackByFn(index, item) {
    return index;
  }

  onChange(event: EventTarget, index: number) {
        let eventObj: MSInputMethodContext = <MSInputMethodContext> event;
        let target: HTMLInputElement = <HTMLInputElement> eventObj.target;
        let files: FileList = target.files;
        this.files.push(files[0]);
  }

  add(){
    if(this.intCount < 15) {
      this.qtdInput[this.intCount] = this.intCount;
      this.intCount++;
    }
  }

  del(){
    if(this.intCount > 0 ){
      this.qtdInput.splice(this.intCount, 1);
      this.intCount--;
    }
  }

  editarDica(myForm){
    let dica:Dica = new Dica(0,"","",false,null,[]);
    dica.codD = this.myForm.controls["codD"].value;
    dica.nome = this.myForm.controls["nome"].value;
    let codR = this.myForm.controls["residuo"].value as number;
    for (let r of this.residuoService.residuos){
      if(r.codR == codR){
        dica.residuo = r;
      }
    }
    dica.isReutilizavel = this.myForm.controls["isReutilizavel"].value as Boolean;
    let pairCount = 0;
    for(let i of this.qtdInput){
      //img
      if(this.files[i] != undefined){
        dica.anexos[pairCount] = this.files[i].name;  //img alterada
      } else {
        dica.anexos[pairCount] = this.fileNomesAntes[i];  //mantém a msm
      }
      //txt
      dica.anexos[pairCount+1] = this.myForm.controls["anexos".concat(i)].value;
      pairCount = pairCount + 2;
    }
    //WS: dica

    let upload = true;
    let dicaSeAlterada: Dica;
    let request = new Request(JSON.stringify(dica));
    this.service.updateDica(request).subscribe(
      data =>{
        let msg:MsgResponse = JSON.parse(data.text());
        if(msg.status){
          this.nService.success("Dica", msg.message);
          dicaSeAlterada = JSON.parse(msg.extra);
          dicaSeAlterada.descricaoPath += ('?' + new Date().getTime());      //cache
          this.service.dicas.splice(this.service.dicaEmEditarIdx, 1, dicaSeAlterada);
          //WS: upload IMG
          let entered = false;
          let postData = {cod:dicaSeAlterada.codD, tipo:"DIC", id:this.cookieS.get("id"), t:this.cookieS.get("token")};
          for(let file of this.files) {
            this.upService.postWithFile(null, postData, file)
              .then((response) => {
                let msg2:MsgResponse = JSON.parse(JSON.stringify(response));
                entered = true;
                if(msg2.status){
                  //console.log("Fileposting success: " + msg2.message);
                } else {
                  console.log("Fileposting error: " + msg2.message);
                }
              })
              .catch((error) => {
                console.log("Fileposting error: " + error);
              });
          }
          // fim upload imgs dica
        } else {
          this.nService.error("Dica", msg.message);
        }
      },
      error => {
        this.nService.error("Dica - erro!", error);
        console.log("Erro no cadastro da dica: " + JSON.stringify(dica));
      }
    )

  }

}

