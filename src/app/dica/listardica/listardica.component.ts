import { Request } from './../../model/request';
import { MsgResponse } from './../../model/msg-response';
import { NotificationsService } from 'angular2-notifications';
import { Dica } from './../../model/dica';
import { DicaService } from './../dica.service';
import { Component, OnInit, Input } from '@angular/core';
import { NgModule, Directive } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';

@Component({
  selector: 'app-listardica',
  templateUrl: './listardica.component.html',
  styleUrls: ['./listardica.component.css']
})


export class ListardicaComponent implements OnInit {
  private dicas:Dica[];
  private termo;

  constructor(private service: DicaService,
              private nService: NotificationsService) { }

  ngOnInit() {
    this.service.getDicas().subscribe(
    data => {
      this.service.dicas = JSON.parse(data.text());
    });
  }

  trackByFn(index, dica) {
    return dica.codD;
  }

  chamarEditar(dica:Dica, i:number){
    //console.log(JSON.stringify(dica));
    this.service.passarDadosEditar(dica, i);
  }

  deletaDica(dica:Dica, i:number){
    if(confirm("Deseja deletar a dica "+ dica.codD +"?")) {
      let request = new Request(JSON.stringify(dica));
      this.service.deletaDica(request).subscribe(
      data => {
        let msg:MsgResponse = JSON.parse(data.text());
        if(msg.status){
          this.service.dicas.splice(i, 1);
          this.nService.success("Dica Deletada!", msg.message);
          this.ngOnInit();
        } else {
          this.nService.error("Erro!", msg.message);
        }
      },
      error =>{
        this.nService.error("Erro!", error);
      }
      );
    }

  }

}
