import { DicaService } from './dica.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormBuilder, FormGroup, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { DicaComponent } from './dica.component';
import { ListardicaComponent } from './listardica/listardica.component';
import { CadastrardicaComponent } from './cadastrardica/cadastrardica.component';
import { EditardicaComponent } from './editardica/editardica.component';
import { MenuModule } from '../menu/menu.module';
import { PipeGlobalModule } from "app/pipe-global/pipe-global.module";


@NgModule({
  declarations: [
    DicaComponent, ListardicaComponent, CadastrardicaComponent, EditardicaComponent
  ],
  imports: [
    ReactiveFormsModule,
    FormsModule,
    CommonModule,
    MenuModule,
    PipeGlobalModule
  ],
  exports: [DicaComponent, ListardicaComponent, CadastrardicaComponent, EditardicaComponent],
  providers: []
})
export class DicaModule {

}
