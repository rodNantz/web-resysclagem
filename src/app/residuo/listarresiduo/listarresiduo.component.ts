import { Request } from './../../model/request';
import { MsgResponse } from './../../model/msg-response';
import { NotificationsService } from 'angular2-notifications';
import { Residuo } from './../../model/residuo';
import { ResiduoService } from './../residuo.service';
import { Component, OnInit } from '@angular/core';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { NgModule } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { Http, Response, Headers, RequestOptions } from "@angular/http";
import 'rxjs/add/operator/map';

@Component({
  selector: 'app-listarresiduo',
  templateUrl: './listarresiduo.component.html',
  styleUrls: ['./listarresiduo.component.css']
})
export class ListarresiduoComponent implements OnInit {
  private postResponse: Response;
  private termo;
  private tinycolor;

  constructor(private service: ResiduoService,
              private nService: NotificationsService) {
    this.tinycolor = require("tinycolor2");
  }

  ngOnInit() {
    
    //this.residuos = this.service.getResiduo();
    this.service.getResiduo().subscribe(
      res => {
        this.service.residuos = res.json();
        this.service.cacheOleAll();
      }, error => {
        console.log(error);
      });
  }
  
  trackByFn(index, residuo) {
    return residuo.codR;
  }

  isColorLight(color): boolean { 
    if (this.tinycolor(color).isLight()) {
      return true;
    }
    return false;
  }

  chamarEditar(residuo:Residuo, idx: number){
    this.service.passarDadosEditar(residuo, idx);
  }

  removerResiduo(residuo, i){
    if(confirm("Você tem certeza que deseja deletar este resíduo: " + residuo.nome + "?")){
      let request = new Request(JSON.stringify(residuo));
      this.service.deletaResiduo(request).subscribe( // antes tava só residuo no parametro
        data =>{
          let msg:MsgResponse = data as MsgResponse;
          if(msg.status){
            this.nService.success("Delete", msg.message);
            this.service.residuos.splice(i, 1);
          } else {
            this.nService.error("Delete", msg.message);
          }
        },
        error => {
          this.nService.error("Delete", error);
          console.error("Erro ao deletar o resíduo: " + error);
        }
      );
    }
  }
}
