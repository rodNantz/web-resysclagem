/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { ResiduoService } from './residuo.service';

describe('ResiduoService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ResiduoService]
    });
  });

  it('should ...', inject([ResiduoService], (service: ResiduoService) => {
    expect(service).toBeTruthy();
  }));
});
