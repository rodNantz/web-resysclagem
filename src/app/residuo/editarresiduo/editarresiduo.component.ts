import { NotificationsService } from 'angular2-notifications';
import { MsgResponse } from './../../model/msg-response';
import { Residuo } from 'app/model/residuo';
import { CookieService } from 'angular2-cookie/core';
import { Request } from './../../model/request';
import { CategoriaService } from './../../categoria/categoria.service';
import { Categoria } from './../../model/categoria';
import { Observable } from 'rxjs/Observable';
import { ResiduoService } from './../residuo.service';
import { UploadService } from '../../upload/upload.service';
import { FormBuilder, FormGroup, FormControl, FormsModule, ReactiveFormsModule, Validators } 
        from '@angular/forms';
import { Component, OnInit, AfterViewInit } from '@angular/core';

@Component({
  selector: 'app-editarresiduo',
  templateUrl: './editarresiduo.component.html',
  styleUrls: ['./editarresiduo.component.css']
})
export class EditarresiduoComponent implements OnInit {
  residuoForm: FormGroup;
  public fileList: FileList;
  public imagemAnterior: string;

  constructor(public fb: FormBuilder, 
              public service: ResiduoService, 
              public categoriaService: CategoriaService,
              public cookieS: CookieService,
              public nService: NotificationsService,
              public uploadService: UploadService) { }

  ngOnInit() {
    this.residuoForm = this.fb.group({
      codR: [],
      nome: [],
      descricao: [],
      //imagemResiduo: ['', Validators.required],
      categoria: []
    });
    this.editarResSetup(event);
  }

  editarResSetup(event){
    this.imagemAnterior = this.service.resEmEditar.imagemResiduo;
  }

  fileChange(event){
    this.fileList = event.target.files;
  }

  editarResiduo(residuoForm) {
    let residuo:Residuo = residuoForm;
    let codC = this.residuoForm.controls["categoria"].value;
    residuo.categoria = this.categoriaService.getCategoriaDeCod(codC);
    if (this.fileList == undefined){
      residuo.imagemResiduo = this.imagemAnterior;
    } else {
      residuo.imagemResiduo = this.fileList[0].name;
    }
    console.log(JSON.stringify(residuo));
    let request = new Request(JSON.stringify(residuo));
    this.service.updateResiduo(request).subscribe(
      data =>{
        let msg:MsgResponse = JSON.parse(data.text());
        if(msg.status){
          this.nService.success("Editar",msg.message);
          //atualizar view
          residuo = JSON.parse(msg.extra);
          //this.service.mudandoImg = true;
          if(this.fileList != undefined && this.fileList.length > 0){
            let file = this.fileList[0]; //let file = event.srcElement.files;
            let postData = {tipo:"RES", 
                            cod:residuo.codR, 
                            id:this.cookieS.get("id"), 
                            t:this.cookieS.get("token")}; 
            this.uploadService.postWithFile(null, postData, file).then(
            response => {
              let msg2:MsgResponse = JSON.parse(JSON.stringify(response));
                if(msg2.status){
                  console.log("Fileposting success: " + msg2.message + " | " + msg2.extra);
                  this.nService.success("Upload","Upload feito!");
                } else {
                  console.log("Fileposting error: " + msg2.message + " | " + msg2.extra);
                  this.nService.error("Upload",msg2.message);
                }
                this.dpsUploadFinally(residuo);
            }, error => {
              this.nService.error("Upload","Upload não realizado!");
              console.log(error);
              this.dpsUploadFinally(residuo);
            });
          } else {
            this.dpsUploadFinally(residuo);
          }
        } else {
          this.nService.error("Editar",msg.message);
        }
        
      },
      error =>{
        this.nService.error("Editar", error);
      }
    );
    
  }

  dpsUploadFinally(residuo: Residuo){
    this.service.residuos.splice(this.service.resEmEditarIdx, 1, residuo);  
    // dibra o cache
    this.service.imagemResiduoNoCache.splice(
      this.service.resEmEditarIdx, 1, 
      this.service.cacheOle(residuo.imagemResiduo)
    );  
    //this.service.mudandoImg = false;
  }

}
