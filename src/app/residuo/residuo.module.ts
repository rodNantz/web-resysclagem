import { BrowserModule } from '@angular/platform-browser';
import { CategoriaService } from './../categoria/categoria.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EditarresiduoComponent } from './editarresiduo/editarresiduo.component';
import { ResiduoComponent } from './residuo.component';
import { ListarresiduoComponent } from './listarresiduo/listarresiduo.component';
import { FormBuilder, FormGroup, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { MenuModule } from '../menu/menu.module';
import { ResiduoService } from './residuo.service';
import { UploadModule } from '../upload/upload.module';
import { UploadService } from '../upload/upload.service';
import { CadastrarresiduoComponent } from './cadastrarresiduo/cadastrarresiduo.component';
import { PipeGlobalModule } from "app/pipe-global/pipe-global.module";
//import { ToastModule } from 'ng2-toastr/ng2-toastr';

@NgModule({
  imports: [
    ReactiveFormsModule,
    FormsModule,
    CommonModule,
    MenuModule,
    UploadModule,
    PipeGlobalModule
  ],
  providers:[
    
  ],
  declarations: [
    EditarresiduoComponent, 
    ResiduoComponent, 
    ListarresiduoComponent, 
    CadastrarresiduoComponent
    ],
  exports: [
    ResiduoComponent, 
    ListarresiduoComponent, 
    EditarresiduoComponent,
    CadastrarresiduoComponent
    ]
})
export class ResiduoModule { }
