import { MsgResponse } from './../../model/msg-response';
import { Request } from './../../model/request';
import { SimpleNotificationsModule, NotificationsService } from 'angular2-notifications';
import { UsuarioService } from './../usuario.service';
import { Usuario } from './../../model/usuario';
import { Component, OnInit, Injectable } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, FormsModule, ReactiveFormsModule, Validators } 
        from '@angular/forms';
import { Http, Response, Headers, RequestOptions } from "@angular/http";
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';

@Component({
  selector: 'app-cadastrarusuario',
  templateUrl: './cadastrarusuario.component.html',
  styleUrls: ['./cadastrarusuario.component.css']
})
export class CadastrarusuarioComponent implements OnInit {
  cadastrarAdminForm: FormGroup;
  private postResponse: Response;
  public nome;
  public email;
  public senha;
  public valor;
  //notifs
  public options = {
    position: ["upper", "left"],
    timeOut: 5000,
    lastOnBottom: true
  };
  constructor(public fb: FormBuilder, 
              private service: UsuarioService, 
              private nService: NotificationsService) { }
  
  ngOnInit() {
    this.cadastrarAdminForm = this.fb.group({
      nome: ["", Validators.required],
      email: ["", Validators.required],
      senha: ["", Validators.required]
    });

    //this.getUsuario
  }

  validarUsuario(admin:Usuario){
    if(admin.nome == "") {
      this.nService.warn("Validação", "Insira um nome!");
      return false;
    }
    if(admin.email == "") {
      this.nService.warn("Validação", "Insira um e-mail!");
      return false;
    }
    if(admin.senha == "") {
      this.nService.warn("Validação", "Insira uma senha!");
      return false;
    }
    return true;
  }

  cadastrarAdmin(usuarioForm) {
    let usuario:Usuario = usuarioForm as Usuario;
    console.log(usuario);
    console.log(this.cadastrarAdminForm.value);
    if(this.validarUsuario(usuario)) {
      // hash
      var createHash = require('sha.js')
      var sha256 = createHash('sha256');
      var hash = sha256.update(usuario.senha, 'utf8').digest('hex')
      usuario.senha = hash;
      //
      let request = new Request(JSON.stringify(usuario));
      //let user = {u: usuario};
      this.service.novoUsuario(request).subscribe(
        data=>{
          let msg:MsgResponse = JSON.parse(data.text());
          if(msg.status) {
            this.nService.success("Cadastrado com sucesso!", msg.message);
            let nUsu:Usuario = JSON.parse(msg.extra);
            this.service.usuarios.push(nUsu);
            this.cadastrarAdminForm.reset();
          } else {
            this.nService.error("Não cadastrado", msg.message);
          }
        },
        error => {
          console.error("Erro ao cadastrar o administrador!");
          this.nService.error("Cadastro não realizado!", error);
        }
      );
    }  
  }

}
