import { Usuario } from './../model/usuario';
import { RouterModule, Routes } from '@angular/router';
import { CadastrarusuarioComponent } from './cadastrarusuario/cadastrarusuario.component';
import { UsuarioService } from './usuario.service';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { BuscaComponent } from './busca/busca.component';
import { UsuariocComponent } from './usuarioc.component';
import { EditarusuarioComponent } from './editarusuario/editarusuario.component';
import { ListarusuarioComponent } from './listarusuario/listarusuario.component';
import { FormBuilder, FormGroup, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { MenuModule } from '../menu/menu.module';

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    ReactiveFormsModule,
    MenuModule
  ],
  declarations: [BuscaComponent, UsuariocComponent, EditarusuarioComponent, ListarusuarioComponent, CadastrarusuarioComponent],
  providers: [],
  exports: [BuscaComponent, UsuariocComponent, EditarusuarioComponent, ListarusuarioComponent, 
            CadastrarusuarioComponent, RouterModule ]
})
export class UsuarioModule {

 }
