echo "building to /dest (...)"
ng build --base-href /

echo "copying to /server/public (...)"
cp -a dist/. server/public/

echo "removing content on /dist for free space"
rm -rf dist/*

echo "done"
